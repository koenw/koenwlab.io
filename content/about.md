---
title: "About me"
published: true
---

{{< figure src="/images/me-april-2021.jpeg" caption="" >}}

I am Koen Westendorp, and this is my site. Thanks for stopping by.

I love learning new things and creating cool stuff using those insights and skills.

Currently I am studying biology at the University of Groningen, The Netherlands. I have a keen interest in complex systems, which I believe is the reason that the study of life brings me so much joy.

Besides biology there is a large number of things that I find interesting. Art is a main theme within those interests, as well as programming and computer science. You can find some of my projects described in [blog posts](/posts) on this site, and on my [GitHub page](https://github.com/koenwestendorp).

## Where to find me
- On [GitHub](https://github.com/koenwestendorp).
- Email: the gmail address you can derive from 'ss', sandwiched between my first and then last name.

## Skills
### Software
- Figma
- Sketch
- Affinity Designer
- Adobe Illustrator and Photoshop
- Logic Pro
- Ableton Live

- Linux (Arch, btw)

- I can type, fast. (around 90-100 WPM, Dvorak)

### Languages
#### For humans
- Dutch (mother tongue)
- English
- Enough German to kinda help myself when presented with a German wiki page

#### For machines
- Rust
- Swift
- Go
- Markdown (my preferred style of writing anything)
- HTML & CSS/Sass
- R
- Python

I have quite a bit of experience in front-end design and development. To me, building high-performance, robust, simple, and elegant websites is very enjoyable. Besides front-end work, I also love creating fast, extensible cli tools, for which I often use Rust and shell scripts.

My main operating system is Arch Linux, but I am intimately familiar with MacOS as well. On Arch, I use the bspwm window manager. 
