---
appearences: Koen Westendorp
date: "2019-11-29T20:55:17Z"
duration: 10
file: critical-t-v1.mp3
published: true
title: 'TRACK: Critical t (v1)'
---

{{< figure src="/images/Critical%20t%20-%20Single.jpg" caption="" >}}

{% include audio.html url='/assets/music/01%20Critical%20t%20v1.mp3' caption='<i>Critical t</i>, created on November 29, 2019.' %}

Currently I have a course on Biostatistics and after an inhumane number of assignment on the topic of--among other things--the critical t (t&ast;) I just had to get these statistical ramblings out of my head. I felt like making a somewhat hazy, dub-like atmosfere that would reflect the insufferable uncertainty that statistics continually confronts you with. Slowly a hasty and hectic, though touching arpegiator steps towards you, symbolizing the structured and repettitive nature of this mathematical realm we students are trying to uncover. A high and varying level of saturation, and the subsequent bothered distortion it invokes, ties the pressing yet expansive fog-filled space together. 

Well, I hope so at least. There is still a *lot* of balancing and refinement to be done.

De groete, Koen
